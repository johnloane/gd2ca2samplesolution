package dkit.gd2.ca2sample;

public enum SortType {
    Ascending(1),
    Descending(-1);

    private int direction;

    public int getDirection(){
        return this.direction;
    }

    private SortType(int direction){
        this.direction = direction;
    }
}
