package dkit.gd2.ca2sample;

import java.util.ArrayList;
import java.util.Collections;

/**
 * An example to demonstrate the use of Comparable, Comparator and Measurable.
 * We use
 * Comparable and Comparator to sort a list
 * Measurer, Measurable to get some numeric representation of an object
 * e.g getAverage()
 */
public class MainApp {
    public static void main(String[] args) {
        MainApp theApp = new MainApp();
        theApp.start();
    }

    public void start()
    {
        //initialize an arraylist to store objects of type Vehicle
        //Remember that Cars and Airplanes are vehicles
        ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>();

        //add some data
        vehicleList.add(new Car("Hyundai", "i10", 1.0, 4, 5));
        vehicleList.add(new Airplane("Boeing", "777", 50, 380, 6000, 39000, 500));
        vehicleList.add(new Car("BMW", "520", 1.8, 5, 5));

        print(vehicleList);

        System.out.println("Sort the list using the Vehicle::compareTo by engine size...");
        Collections.sort(vehicleList);
        print(vehicleList);

        System.out.println("Sort the list using the VehicleMakeComparator by make...");
        int direction = 1;
        Collections.sort(vehicleList, new VehicleMakeComparator(direction));
        print(vehicleList);

        System.out.println("Sort the list in reverse order using the VehicleMakeComparator by make...");
        direction = -1;
        Collections.sort(vehicleList, new VehicleMakeComparator(direction));
        print(vehicleList);

        System.out.println("Sort the list using the VehicleModelComparator by model...");
        Collections.sort(vehicleList, new VehicleModelComparator(SortType.Ascending));
        print(vehicleList);

        System.out.println("Get the largest vehicle in the list by passenger size");
        Vehicle largestVehicle = getLargest(vehicleList, new VehiclePassengerSizeMeasurer());
        System.out.println(largestVehicle);
    }

    public void print(ArrayList<Vehicle> list){
        for(Vehicle v : list){
            System.out.println(v.toString());
        }
    }

    public Vehicle getLargest(ArrayList<Vehicle> list, IMeasurer measure){
        double largest = Double.MIN_VALUE;
        double actualValue = 0;
        Vehicle largestObj = null;

        for(Vehicle v : list){
            //read the value from the vehicle
            actualValue = measure.getValue(v);

            if(actualValue > largest){
                largest = actualValue;
                largestObj = v;
            }
        }
        //return the largest objects
        return largestObj;

    }
}
