package dkit.gd2.ca2sample;

import java.util.Comparator;

/**
 * Used in the MainApp with Collections.sort() to specify how to sort
 * an ArrayList of Vehicles
 */

public class VehicleMakeComparator implements Comparator<Vehicle> {
    // Used to reverse the sort i.e A-Z or Z-A
    private int direction;

    public VehicleMakeComparator(int direction){
        this.direction = direction;
    }

    public int compare(Vehicle v1, Vehicle v2){
        return direction * v1.getMake().compareToIgnoreCase(v2.getMake());
    }

}
