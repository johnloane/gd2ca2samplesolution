package dkit.gd2.ca2sample;

public class VehiclePassengerSizeMeasurer implements IMeasurer {
    public double getValue(Object o) {
        Vehicle v = (Vehicle)o;
        return v.getPassengerSize();
    }
}
