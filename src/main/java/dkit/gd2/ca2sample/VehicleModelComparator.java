package dkit.gd2.ca2sample;

import java.util.Comparator;

/* This allows us to compare two vehicles based on their model
   Also uses a SortType enum to allow us to change the sort order
 */
public class VehicleModelComparator implements Comparator<Vehicle> {
    private SortType sortType;

    /*Compare the use of the enum approach to the approach used
    in VehicleMakeComparator where we pass an integer to specify direction. If we allow the user to enter an integer then
    a) It is hard for the user to know which direction is which so
    does 1 mean A-Z or Z-A?
    b) It also means that the user can type in any integer
     */

    public VehicleModelComparator(SortType sortType){
        this.sortType = sortType;
    }

    public int compare(Vehicle v1, Vehicle v2){
        int direction = sortType.getDirection();
        return direction * v1.getModel().compareToIgnoreCase(v2.getModel());
    }
}
