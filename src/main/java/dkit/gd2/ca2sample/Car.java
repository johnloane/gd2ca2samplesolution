package dkit.gd2.ca2sample;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Car extends Vehicle{
    private int doorCount;

    public Car(String make, String model, double engineSize, int passengerSize, int doorCount) {
        super(make, model, engineSize, passengerSize);
        this.doorCount = doorCount;
    }

    public int getDoorCount() {
        return doorCount;
    }

    @Override
    public String toString() {
        String carString = "Car: \t\t";
        carString += super.toString();
        carString += "\t doorCount = " + doorCount;
        return carString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        if (!super.equals(o)) return false;

        Car car = (Car) o;

        return getDoorCount() == car.getDoorCount();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getDoorCount();
        return result;
    }

    public void writeCarToFile() throws IOException{
        try(BufferedWriter carFile = new BufferedWriter(new FileWriter("car.txt"))) {
            carFile.write(this.getMake() + "," + this.getModel() + ","+ this.getEngineSize() + "," + this.getPassengerSize() + "," + this.getDoorCount());
        }
    }

    public void readCarFromFile() throws IOException{
        try(Scanner sc = new Scanner(new BufferedReader(new FileReader("car.txt")))){
            sc.useDelimiter(",");
            while(sc.hasNextLine()){
                String make = sc.next();
                sc.skip(sc.delimiter());
                String model = sc.next();
                double engineSize = sc.nextDouble();
                sc.skip(sc.delimiter());
                int passengerNumber = sc.nextInt();
                sc.skip(sc.delimiter());
                int numDoors = sc.nextInt();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
