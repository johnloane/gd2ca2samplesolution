package dkit.gd2.ca2sample;


import java.util.Objects;

/**
 * A common parent class for cars and airplanes.
 */

public class Vehicle implements Comparable<Vehicle>{
    private String make, model;
    private double engineSize;
    private int passengerSize;

    public Vehicle(String make, String model, double engineSize, int passengerSize) {
        this.make = make;
        this.model = model;
        this.engineSize = engineSize;
        this.passengerSize = passengerSize;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public int getPassengerSize() {
        return passengerSize;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", engineSize=" + engineSize +
                ", passengerSize=" + passengerSize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;

        Vehicle vehicle = (Vehicle) o;

        if (Double.compare(vehicle.getEngineSize(), getEngineSize()) != 0) return false;
        if (getPassengerSize() != vehicle.getPassengerSize()) return false;
        if (getMake() != null ? !getMake().equals(vehicle.getMake()) : vehicle.getMake() != null) return false;
        return getModel() != null ? getModel().equals(vehicle.getModel()) : vehicle.getModel() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getMake() != null ? getMake().hashCode() : 0;
        result = 31 * result + (getModel() != null ? getModel().hashCode() : 0);
        temp = Double.doubleToLongBits(getEngineSize());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + getPassengerSize();
        return result;
    }

    public int compareTo(Vehicle o) {
        /* Because we are comparing double values then we must use
        and if else. The general rule us
        byte, short , int, long use a simple subtraction of the values
        float, double use and if else like below
        String - use a compareTo() or compareToIgnoreCase()
        see VehicleMakeComparator()
         */
        double diff = this.engineSize - o.getEngineSize();
        if(diff > 0){
            return 1;
        }
        else if(diff < 0){
            return -1;
        }
        else {
            return 0;
        }
    }
}
