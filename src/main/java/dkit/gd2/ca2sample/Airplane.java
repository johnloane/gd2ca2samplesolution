package dkit.gd2.ca2sample;

public class Airplane extends Vehicle {
    private double maxRange, maxAltitude, maxSpeed;

    public Airplane(String make, String model, double engineSize, int passengerSize, double maxRange, double maxAltitude, double maxSpeed) {
        super(make, model, engineSize, passengerSize);
        this.maxRange = maxRange;
        this.maxAltitude = maxAltitude;
        this.maxSpeed = maxSpeed;
    }

    public double getMaxRange() {
        return maxRange;
    }

    public double getMaxAltitude() {
        return maxAltitude;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "maxRange=" + maxRange +
                ", maxAltitude=" + maxAltitude +
                ", maxSpeed=" + maxSpeed +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airplane)) return false;
        if (!super.equals(o)) return false;

        Airplane airplane = (Airplane) o;

        if (Double.compare(airplane.getMaxRange(), getMaxRange()) != 0) return false;
        if (Double.compare(airplane.getMaxAltitude(), getMaxAltitude()) != 0) return false;
        return Double.compare(airplane.getMaxSpeed(), getMaxSpeed()) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(getMaxRange());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxAltitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMaxSpeed());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
