package dkit.gd2.ca2sample;
/* We use this interface whenever we want to do things like
    getAverage(), getLargest() - see MainApp for an example
 */
public interface IMeasurer {
    //Notice that this method takes an object and not a Vehicle
    //so we can use it for lots of objects
    public double getValue(Object o);
}
